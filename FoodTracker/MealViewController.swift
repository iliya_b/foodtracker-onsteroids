//
//  ViewController.swift
//  FoodTracker
//
//  Created by Iliya Bahchevanski on 19.07.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import UIKit
import os.log
import FacebookShare
import FacebookCore


class MealViewController: UIViewController, UITextFieldDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: Propertie
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mealNameLable: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var currentMeal: Meal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        
        if let meal = currentMeal {
            navigationItem.title = meal.name
            nameTextField.text = meal.name
            photoImageView.image = meal.photo
            ratingControl.rating = meal.rating
        }
        
        
        updateSaveButtonState()
        
    }
    
    //MARK: Navigation
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        let isPresentingInAddMeal = presentingViewController is UINavigationController
        
        if isPresentingInAddMeal {
            dismiss(animated: true, completion: nil)
        } else if let owningNavigtaionControll = navigationController {
            owningNavigtaionControll.popViewController(animated: true)
        } else {
            fatalError("Is not inside a navigtaion controller")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        super.prepare(for: segue, sender: sender)
        
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling...", log: OSLog.default,
                   type: .debug)
            return
        }
        
        
        let name = nameTextField.text ?? ""
        let photo = photoImageView.image
        let rating = ratingControl.rating
        let id: UUID
        
        if let meal = currentMeal {
            id = meal.mealId
        } else {
            id = UUID()
        }
        
        currentMeal = Meal(name: name, photo: photo, rating: rating, id: id)
    }
    
    
    //MARK: Actions
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer) {
        //hide keyboard
        nameTextField.resignFirstResponder()
        
        //controller to pick media
        let imagePickerController = UIImagePickerController()
        
        //select only from library
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        
        //notify when user selects an  image
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    @IBAction func shareOnFacebook(_ sender: UIButton) {
        
        if let imageToBeShared = photoImageView.image {
            let photo = FacebookShare.Photo(image: imageToBeShared, userGenerated: true)
            let content = FacebookShare.PhotoShareContent(photos: [photo])
            do {
                try FacebookShare.ShareDialog.show(from: self,content: content)
            } catch let currentError {
                print(currentError.localizedDescription)
                self.showCustomAlert(message: "Unable to post image to facebook!")
            }
        }
    }
    
    private func showCustomAlert(message: String){
        let alert = UIAlertController(title: "Alert", message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click",
                                      style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = textField.text
    }
    
    
    //MARK: UIImagePcikerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as?
            UIImage else {
                fatalError("Expected a dictionary containing an image, but was provided but was provided \(info)")
        }
        
        photoImageView.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Private Methods
    private func updateSaveButtonState(){
        let text = nameTextField.text ?? " "
        saveButton.isEnabled = !text.isEmpty
    }
}

