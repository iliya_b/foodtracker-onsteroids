//
//  MealTableViewController.swift
//  FoodTracker
//
//  Created by Iliya Bahchevanski on 26.07.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import UIKit
import os.log

class MealTableViewController: UITableViewController, UIPopoverPresentationControllerDelegate, PressedButtonInPopOverProtocol, UIGestureRecognizerDelegate {
    
    struct CellSnapshot {
        static var mySnapshotOfaCell: UIView? = nil
    }
    
    struct PathOfCell {
        static var initialIndexPath: IndexPath? = nil
    }
    
    
    //MARK: Properties
    var meals = [Meal]()
    var userSortChoice: String?
    private var isUserEditing: Bool = false
    let myMealsLoader = MealsLoader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = editButtonItem
        
        if let savedMeals = myMealsLoader.loadMeals() {
            meals += savedMeals
        } else {
            meals += myMealsLoader.loadSampleMeals()
        }
        
        let tapGesture = UILongPressGestureRecognizer(target: self, action: #selector(longGestureActionForCell))
        tableView.addGestureRecognizer(tapGesture)
        tapGesture.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return meals.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MealTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,for: indexPath) as? MealTableViewCell else {
            fatalError("Unable to dequeue an instance of MealTableViewCell")
        }
        
        let meal = meals[indexPath.row]
        
        cell.nameLabel.text = meal.name
        cell.photoImageView.image = meal.photo
        cell.ratingControl.rating = meal.rating
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            meals.remove(at: indexPath.row)
            myMealsLoader.saveMeals(meals: meals)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        
    }
    
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let mealToMove = meals[fromIndexPath.row]
        meals.remove(at: fromIndexPath.row)
        meals.insert(mealToMove, at: to.row)
        tableView.reloadData()
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch (segue.identifier ?? " "){
        case "AddItem":
            os_log("Adding a new meal", log: OSLog.default, type: .debug)
        case "ShowDetail":
            guard let mealDetalViewController = segue.destination as? MealViewController else {
                fatalError("Unexpected destinatnion \(segue.destination)")
            }
            
            guard let selectedMealCell = sender as? MealTableViewCell else {
                fatalError("Unexpected sender \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedMealCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedMeal = meals[indexPath.row]
            mealDetalViewController.currentMeal = selectedMeal
            
        case "SearchForMeal":
            guard let mySearchClass = segue.destination as? SearchForMealsTableViewController else {
                fatalError("Wrong seague for SearchForMeals!")
            }
            
            mySearchClass.myMeals = meals
            
        case "showPopOver":
            let sortByPopoverClass = segue.destination as? SortByPopOverViewController
            sortByPopoverClass?.pressedButton = userSortChoice
            sortByPopoverClass?.delegate = self
            
            let popOverController = segue.destination.popoverPresentationController
            popOverController?.delegate = self
            popOverController?.sourceRect = CGRect(x: 0, y: 30, width: 0, height: 0)
            
        default:
            fatalError("Unexpected identifier \(String(describing: segue.identifier))")
        }
        
    }
    
    //MARK: Actions
    @IBAction func unwindToMealList(sender: UIStoryboardSegue){
        
        if let sourceViewController = sender.source as? MealViewController,
            let meal = sourceViewController.currentMeal {
            
            if let searchedIndex = meals.index(where: { $0.mealId == meal.mealId}){
                
                let selectedIndexPath = IndexPath(row: searchedIndex, section: 0)
                meals[searchedIndex] = meal
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
                
            } else {
                
                let newIndexPath = IndexPath(row: meals.count, section: 0)
                meals.append(meal)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            
            myMealsLoader.saveMeals(meals: meals)
            
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController,
                                   traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func saveUserSortChoice(userChoice: String){
        self.userSortChoice = userChoice
        sortMeals(userSortChoice: userSortChoice!)
    }
    
    //cutom drag and drop
    @IBAction func longGestureActionForCell(_ sender: UILongPressGestureRecognizer) {
        let state = sender.state
        let locationInView = sender.location(in: tableView)
        let indexPathOfLocation = tableView.indexPathForRow(at: locationInView)
        
        switch state {
        case UIGestureRecognizerState.began:
            if indexPathOfLocation != nil {
                PathOfCell.initialIndexPath = indexPathOfLocation
                let cell = tableView.cellForRow(at: indexPathOfLocation!) as? MealTableViewCell
                CellSnapshot.mySnapshotOfaCell = snapshotOfCell(input: cell!)
                //CellSnapshot.mySnapshotOfaCell = cell
                var center = (cell?.center)!
                CellSnapshot.mySnapshotOfaCell?.center = center
                CellSnapshot.mySnapshotOfaCell?.alpha = 0.0
                tableView.addSubview(CellSnapshot.mySnapshotOfaCell!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    CellSnapshot.mySnapshotOfaCell?.center = center
                    CellSnapshot.mySnapshotOfaCell?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    CellSnapshot.mySnapshotOfaCell?.alpha = 0.98
                    cell?.alpha = 0.0
                }, completion: { (finished) -> Void in
                    if finished {
                        cell?.isHidden = true
                    }
                    
                })
            }
            
        case UIGestureRecognizerState.changed:
            var center = CellSnapshot.mySnapshotOfaCell?.center
            center?.y = locationInView.y
            CellSnapshot.mySnapshotOfaCell?.center = center!
            
            if ((indexPathOfLocation != nil) && (indexPathOfLocation != PathOfCell.initialIndexPath)) {
                swap(&meals[(indexPathOfLocation?.row)!], &meals[(PathOfCell.initialIndexPath?.row)!])
                tableView.moveRow(at: PathOfCell.initialIndexPath!, to: indexPathOfLocation!)
                PathOfCell.initialIndexPath = indexPathOfLocation
            }
            
        default:
            let cell = tableView.cellForRow(at: indexPathOfLocation!)
            cell?.isHidden = false
            cell?.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                CellSnapshot.mySnapshotOfaCell?.center = (cell?.center)!
                CellSnapshot.mySnapshotOfaCell?.transform = CGAffineTransform.identity
                CellSnapshot.mySnapshotOfaCell?.alpha = 0.0
                cell?.alpha = 1.0
            }, completion: { (finished) -> Void in
                if  finished {
                    PathOfCell.initialIndexPath = nil
                    CellSnapshot.mySnapshotOfaCell?.removeFromSuperview()
                    CellSnapshot.mySnapshotOfaCell = nil
                }
                
            })
        }
        
    }
    
    
    //MARK: Private methods
    private func snapshotOfCell(input: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(input.bounds.size, false, 0.0)
        input.layer.render(in: UIGraphicsGetCurrentContext()!)
        let iamge = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cellSnapshot: UIView = UIImageView(image: iamge)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    
    private func sortMeals(userSortChoice: String){
        
        switch userSortChoice {
        case "name":
            meals.sort(by: { $0.name > $1.name})
        case "rating":
            meals.sort(by: {$0.rating > $1.rating})
        default:
            print("unknown choice for sorting")
            break
        }
        tableView.reloadData()
    }
    
    
}
