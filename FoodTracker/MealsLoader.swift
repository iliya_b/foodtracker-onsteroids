//
//  MealsLoader.swift
//  FoodTracker
//
//  Created by Iliya Bahchevanski on 24.08.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import UIKit
import os.log

class MealsLoader
{

     func loadSampleMeals()-> [Meal]{
        let photo1 = UIImage(named: "meal1")
        let photo2 = UIImage(named: "meal2")
        let photo3 = UIImage(named: "meal3")
        let uuid1 =  UUID()
        let uuid2 =  UUID()
        let uuid3 =  UUID()
        
        
        guard let meal1 = Meal(name: "Caprses Salad", photo: photo1, rating: 4, id: uuid1) else {
            fatalError("Unable to init meal1")
        }
        
        guard let meal2 = Meal(name: "Chicken and Potatos", photo: photo2, rating: 5, id: uuid2) else {
            fatalError("Unable to init meal2")
        }
        
        guard let meal3 = Meal(name: "Past with Meatballs", photo: photo3, rating: 3, id: uuid3) else {
            fatalError("Unable to init meal3")
        }
        
        return [meal1, meal2, meal3]
        
    }
    
    func saveMeals(meals toSave: [Meal]){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(toSave, toFile: Meal.ArchiveURl.path)
        
        if isSuccessfulSave {
            os_log("Meal successfully saved ", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save meals... ", log: OSLog.default, type: .error)
        }
        
    }
    
     func loadMeals()->[Meal]?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: Meal.ArchiveURl.path) as? [Meal]
    }
    
}
