//
//  SortByPopOverViewController.swift
//  FoodTracker
//
//  Created by Iliya Bahchevanski on 06.08.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import UIKit


protocol PressedButtonInPopOverProtocol {
    func saveUserSortChoice(userChoice: String)
}

class SortByPopOverViewController: UIViewController {

    @IBOutlet weak var byRatingButton: UIButton!
    @IBOutlet weak var byNameButton: UIButton!
    var pressedButton: String?
    var delegate: PressedButtonInPopOverProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
    }
    
    //MARK: Actions

    @IBAction func byRatingTouched(_ sender: UIButton) {
        pressedButton = "rating"
        saveChoiceAndQuitPopOver(userChoice: pressedButton!)
        
    }

    @IBAction func byNameAction(_ sender: UIButton) {
        pressedButton = "name"
        saveChoiceAndQuitPopOver(userChoice: pressedButton!)
    }
    
    //MARK: Private functions
    private func saveChoiceAndQuitPopOver(userChoice: String){
        delegate?.saveUserSortChoice(userChoice: userChoice)
        self.dismiss(animated: false, completion: nil)
    }
    
    
}

