//
//  Meal.swift
//  FoodTracker
//
//  Created by Iliya Bahchevanski on 24.07.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import UIKit
import os.log


class Meal: NSObject, NSCoding {
    
    //MARK: Properties
    var name: String
    var photo: UIImage?
    var rating: Int
    let mealId: UUID
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory,in: .userDomainMask).first!
    static let ArchiveURl = DocumentsDirectory.appendingPathComponent("meals")
    
    //MARK: Types
    struct PropertyKeys {
        static let name = "name"
        static let photo = "photo"
        static let raiting = "raiting"
        static let id = "id"
    }
    
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKeys.name) as? String
            else {
                os_log("Unable to devoce the name for a Meal object", log: OSLog.default, type: .debug)
                return nil
        }
        
        let photo = aDecoder.decodeObject(forKey: PropertyKeys.photo) as? UIImage
        let rating = aDecoder.decodeInteger(forKey: PropertyKeys.raiting)
        let id = aDecoder.decodeObject(forKey: PropertyKeys.id) as? UUID
        
        
        self.init(name: name, photo: photo, rating: rating, id: id)
    }
    

    //MARK: Initialization
    init?(name: String, photo: UIImage?, rating: Int, id: UUID?) {


        guard !name.isEmpty else {
            return nil
        }
        
        guard (rating >= 0) && (rating <= 5) else {
            return nil
        }
        
        guard let currentMealId = id else {
            return nil
        }
        
        self.name = name
        self.photo = photo
        self.rating = rating
        self.mealId = currentMealId
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(name, forKey: PropertyKeys.name)
        aCoder.encode(photo, forKey: PropertyKeys.photo)
        aCoder.encode(rating, forKey: PropertyKeys.raiting)
        aCoder.encode(mealId, forKey: PropertyKeys.id)
        
    }
}
