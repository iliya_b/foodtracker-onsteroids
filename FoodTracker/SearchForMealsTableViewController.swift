//
//  SearchForMealsTableViewController.swift
//  FoodTracker
//
//  Created by Iliya Bahchevanski on 29.07.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import UIKit

class SearchForMealsTableViewController: UITableViewController {
    
    //MARK: Properties
    var myMeals = [Meal]()
    var filterMeals = [Meal]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchBar.text != "" {
            return filterMeals.count
        }
        return myMeals.count
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MealTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,for: indexPath) as? MealTableViewCell else {
            fatalError("Unable to dequeue an instance of MealTableViewCell")
        }
        
        //check if user has given an input
        var meal: Meal
        if searchBar.text != "" {
            meal = filterMeals[indexPath.row]
        } else {
            meal = myMeals[indexPath.row]
        }
        
        cell.nameLabel.text = meal.name
        cell.photoImageView.image = meal.photo
        cell.ratingControl.rating = meal.rating
        
        
        return cell
    }
    
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch (segue.identifier ?? " ") {
        case "editMealFromSearch":
            guard let mealDetailViewControlelr = segue.destination as?
                MealViewController else {
                    fatalError("Unexpectd destination \(segue.destination)")
            }
            
            guard let selectedMealCell = sender as? MealTableViewCell else {
                fatalError("Unexpected sender \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedMealCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedMeal: Meal
            
            //maybe the user has not searched for anything
            //but just clicked on a meal
            if filterMeals.count != 0 {
                selectedMeal = filterMeals[indexPath.row]
            } else {
                selectedMeal = myMeals[indexPath.row]
            }
            
            mealDetailViewControlelr.currentMeal = selectedMeal
            
        default:
            fatalError("Unexpected identifier \(String(describing: segue.identifier))")
        }
        
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //MARK: Functions
    func filterContentForSearchText(searchString: String, scope: String = "All") {
        filterMeals = myMeals.filter {
            $0.name.localizedCaseInsensitiveContains(searchString)
        }
        tableView.reloadData()
    }
    
}

//MARK: Extensions
extension SearchForMealsTableViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        filterContentForSearchText(searchString: searchText)
    }
}
