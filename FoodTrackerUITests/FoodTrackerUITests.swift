//
//  FoodTrackerUITests.swift
//  FoodTrackerUITests
//
//  Created by Iliya Bahchevanski on 18.08.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import XCTest

class FoodTrackerUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCallCreateMeal() {
        
        let app = XCUIApplication()
        app.navigationBars["Your Meals"].buttons["Add"].tap()
        
        let textField = app.textFields["Enter meal name"]
        textField.tap()
        textField.typeText("TestMeal")
        app.keyboards.buttons["Done"].tap()
        app.buttons["Set 2 star rating"].tap()
        app.navigationBars["TestMeal"].buttons["Save"].tap()
        XCTAssertTrue(app.tables.staticTexts["TestMeal"].exists)
        
    }
    
    
    func testCallSearchMeal(){
        
        let app = XCUIApplication()
        let yourMealsNavigationBar = app.navigationBars["Your Meals"]
        yourMealsNavigationBar.buttons["Search"].tap()
        let searchMealPlaceHolder = app.tables.searchFields["Search for Meal"]
        XCTAssertTrue(searchMealPlaceHolder.exists)
        
    }
    
    func testCallEdit(){
        
        let yourMealsNavigationBar = XCUIApplication().navigationBars["Your Meals"]
        yourMealsNavigationBar.buttons["Edit"].tap()
        let doneButton = yourMealsNavigationBar.buttons["Done"]
        
        XCTAssertTrue(doneButton.exists)
        
    }
    
    func testEditSearchedMeal(){
        
        let app = XCUIApplication()
        app.navigationBars["Your Meals"].buttons["Search"].tap()
        
        let searchForMealSearchField = app.tables.searchFields["Search for Meal"]
        searchForMealSearchField.tap()
        searchForMealSearchField.typeText("TestMeal")
        //app.cells.allElementsBoundByIndex[0].tap()
        app.tables.staticTexts["TestMeal"].tap()
        let textField = app.textFields["TestMeal"]
        textField.tap()
        textField.typeText("-Edited")
        app.keyboards.buttons["Done"].tap()
        app.navigationBars["TestMeal-Edited"].buttons["Save"].tap()
        XCTAssertTrue(app.tables.staticTexts["TestMeal-Edited"].exists)

    }
    
    func testEditSelectedMeal(){
        
        let app = XCUIApplication()
        app.tables.cells.element(boundBy: 0).tap()
        let staticText = app.staticTexts["Meal Name"]
        XCTAssertTrue(staticText.exists)
        
    }
    
//    func testDeleteMeal(){
//        let app = XCUIApplication()
//        
//        let tablesQuery = app.tables
//        tablesQuery.staticTexts["TestMeal-Edited"].swipeLeft()
//        app.keyboards.buttons["Delete"].tap()
//
//        
//    }
    
}
