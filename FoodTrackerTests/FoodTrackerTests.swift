//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Iliya Bahchevanski on 19.07.17.
//  Copyright © 2017 Iliya Bahchevanski. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {

    //MARK: Meal Class Tests 
    func testMealInitializationSucceeds(){
        
        let zeroRating = Meal(name: "Zero", photo: nil, rating: 0, id: UUID.init())
        XCTAssertNotNil(zeroRating)
        
        
        let positiveRating = Meal(name: "Positive", photo: nil, rating: 5, id: UUID.init())
        XCTAssertNotNil(positiveRating)
        
    }
    
    
    func testMealInitializationFails(){
        let negativeRatingMeal = Meal(name: "Negative", photo: nil, rating: -1,id: UUID.init())
        XCTAssertNil(negativeRatingMeal)
        
        let largeRatingMeal = Meal(name: "Large", photo: nil, rating: 7, id: UUID.init())
        XCTAssertNil(largeRatingMeal)
        
        let emtpyStringMeal = Meal(name: "", photo: nil, rating: 0, id: UUID.init())
        XCTAssertNil(emtpyStringMeal)
    }
    
    func testLoadSampleMeals(){
        let myMealsLoader = MealsLoader()
        
        let sampleMeals = myMealsLoader.loadSampleMeals()
        
        XCTAssertEqual(sampleMeals.count,3)
        
    }
    
    //will fail on first use for the selected device
    func testLoadMeals() {
        let myMealsLoader = MealsLoader()
        
        let loadedMeals = myMealsLoader.loadMeals()
        
        XCTAssertNotNil(loadedMeals)
    }
}
