# Food Tracker on Steroids


## What is this ?
This is is an example of the FoodTracker App from [Apple](https://developer.apple.com/library/content/referencelibrary/GettingStarted/DevelopiOSAppsSwift/). Functionalities like:

* Sorting
* Searching
* Share on Facebook
* Drag 
* UI Tests 

were added. This was just a test and a playground to see how Swift and iOS development works. 
